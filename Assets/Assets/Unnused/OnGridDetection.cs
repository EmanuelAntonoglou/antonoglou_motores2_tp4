using UnityEngine;

public class OnGridDetection : MonoBehaviour
{
    [Header("Components")]
    private Minion minion;
    private TileController tile;

    private void Start()
    {
        minion = transform.parent.GetComponent<Minion>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tile")
        {
            EditModeManager.i.canPlace = true;
            tile = other.GetComponent<TileController>();
            minion.tile = tile;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tile")
        {
            EditModeManager.i.canPlace = false;
        }
    }
}

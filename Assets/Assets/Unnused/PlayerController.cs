using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Edit Mode Data")]
    [NonSerialized] public bool placed = true;

    [Header("Reset Data")]
    private Vector3 originalPos;

    [Header("Reset Data")]
    [NonSerialized] public Rigidbody rb;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        originalPos = transform.position;
    }

    private void Update()
    {
        if (!EditModeManager.i.playMode)
        {
            rb.velocity = Vector3.zero;
        }
    }

    public void Reset()
    {
        transform.position = originalPos;
    }

}

using System.Collections.Generic;
using UnityEngine;

public class MinionManager : MonoBehaviour
{
    public static MinionManager i;

    public delegate void ResetMinionState();
    public static event ResetMinionState OnResetMinionState;

    private void Awake()
    {
        CreateSingleton();
    }

    private void CreateSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeToOriginalStateForAll()
    {
        OnResetMinionState?.Invoke();
    }

}

using System;
using System.Collections;
using UnityEngine;

public class Minion : MonoBehaviour
{
    public enum MinionType { Red, Blue }

    [Header("Minion Type Data")]
    [SerializeField] private MinionType minionType;

    [Header("Minion Movement Data")]
    [SerializeField] private float speed;
    [SerializeField] private float snapValue;
    [SerializeField] private float snapDuration;
    [SerializeField] private float moveDelay;
    [SerializeField] private LayerMask collisionLayers;
    [SerializeField] private LayerMask pusheableCollisionLayers;
    [NonSerialized] public Vector3 originalPos;
    private bool move = false;
    private Vector3 nextPosition;
    private bool variable = true;

    [Header("Edit Mode Data")]
    [NonSerialized] public bool placed = false;
    [NonSerialized] public bool pointerEntered = false;

    [Header("References")]
    [NonSerialized] public TileController tile;

    [Header("Components")]
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        MinionManager.OnResetMinionState += ChangeToOriginalState;
        nextPosition = transform.position + transform.forward * snapValue;
    }

    private void OnDestroy()
    {
        MinionManager.OnResetMinionState -= ChangeToOriginalState;
    }

    private void FixedUpdate()
    {
        if (!EditModeManager.i.playMode)
        {
            move = false;
            rb.velocity = Vector3.zero;

            if (pointerEntered && Input.GetMouseButtonDown(0) && placed)
            {
                gameObject.transform.Rotate(0, 90, 0);
            }
            else if (pointerEntered && Input.GetMouseButtonDown(1))
            {
                //DestroyMinion();
            }
        }
        else
        {
            Move();
        }
    }

    //public void DestroyMinion()
    //{
    //    if (tile != null)
    //    {
    //        tile.taken = false;
    //        tile.ChangeMaterialToNormal();
    //    }
    //    Destroy(gameObject);
    //}

    private void Move()
    {
        if ((minionType == MinionType.Red && TurnManager.i.IsRedTurn()) ||
                (minionType == MinionType.Blue && TurnManager.i.IsBlueTurn()))
        {
            Vector3 direction = transform.forward;
            RaycastHit hitPushable;
            RaycastHit hitObstacle;

            // Detectar objeto empujable en el siguiente tile
            bool hitPushableDetected = Physics.Raycast(transform.position, direction, out hitPushable, snapValue, pusheableCollisionLayers);

            // Detectar obst�culo en el tile despu�s del siguiente
            bool hitObstacleDetected = Physics.Raycast(transform.position + direction * snapValue, direction, out hitObstacle, snapValue, collisionLayers);

            Debug.DrawRay(transform.position, direction * snapValue, hitPushableDetected ? Color.green : Color.red, 1.0f);
            Debug.DrawRay(transform.position + direction * snapValue, direction * snapValue, hitObstacleDetected ? Color.yellow : Color.blue, 1.0f);

            if (variable)
            {
                nextPosition = transform.position + transform.forward * snapValue;
                StartCoroutine(MoveToPosition(nextPosition));
                variable = false;
            }
        }
    }

    private IEnumerator MoveToPosition(Vector3 targetPosition)
    {
        Vector3 startPosition = transform.position;
        float elapsedTime = 0;

        while (elapsedTime < snapDuration)
        {
            if (!EditModeManager.i.playMode)
            {
                yield break;
            }
            rb.MovePosition(Vector3.Lerp(startPosition, targetPosition, elapsedTime / snapDuration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        rb.MovePosition(targetPosition);
    }

    public void OnPointerEnter()
    {
        pointerEntered = true;
    }

    public void OnPointerExit()
    {
        pointerEntered = false;
    }

    public void ChangeToOriginalState()
    {
        transform.position = originalPos;
        rb.velocity = Vector3.zero;
        move = false;
        nextPosition = transform.position;
    }

    private void OnDrawGizmos()
    {
        if (move && EditModeManager.i.playMode)
        {
            Gizmos.color = Color.red;
            Vector3 direction = transform.forward;
            Gizmos.DrawRay(transform.position, direction * snapValue);
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, direction * 2 * snapValue);
        }
    }
}

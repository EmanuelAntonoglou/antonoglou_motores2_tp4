using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractuableObject : MonoBehaviour
{
    [Header("Edit Mode Data")]
    [NonSerialized] public Vector3 originalPos;

    private void Start()
    {
        originalPos = transform.position;
        EditModeManager.OnResetEditState += ChangeToOriginalState;
    }

    private void OnDestroy()
    {
        EditModeManager.OnResetEditState -= ChangeToOriginalState;
    }

    public void ChangeToOriginalState()
    {
        transform.position = originalPos;
        gameObject.SetActive(true);
    }
}

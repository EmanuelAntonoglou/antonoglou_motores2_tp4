using UnityEngine;
using System.Collections.Generic;

public class MaterialController : MonoBehaviour
{
    public static MaterialController i;

    [Header("Material Data")]
    public Material mat1;

    //Delegates
    public delegate void AddGridMaterial(bool add);
    public event AddGridMaterial OnAddGridMaterial;


    private void Awake()
    {
        CreateSingleton();
    }

    private void CreateSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddTilesGridMaterial(bool add)
    {
        OnAddGridMaterial?.Invoke(add);
    }

    


}

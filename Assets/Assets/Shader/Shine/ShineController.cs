using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShineController : MonoBehaviour
{
    [SerializeField] private bool isUI = false;
    [SerializeField] public Material reflectionMat;
    [SerializeField] public Material sprMat;
    [NonSerialized] private Texture sprite;
    [NonSerialized] public Color reflectionColor;
    [NonSerialized] private Color originalColor;

    [Header("References")]
    private SpriteRenderer sprRend;

    private void Awake()
    {
        sprRend = GetComponent<SpriteRenderer>();

        originalColor = sprRend.color;
    }

    private void Start()
    {
        SetReflectionColor();
        if (!isUI)
        {
            ChangeMaterial(reflectionMat, Color.white);
        }
        else
        {
            ChangeMaterial(sprMat, Color.white);
        }
        InitializeMaterial();
    }

    public void InitializeMaterial()
    {
        sprite = sprRend.sprite.texture;
        sprRend.material.SetTexture("_MainTexture", sprite);
        sprRend.material.SetColor("_ReflectionColor", reflectionColor);
        sprRend.material.SetColor("_FillColor", originalColor);
    }

    private void SetReflectionColor()
    {
        if (originalColor == new Color(1, 0, 0))
        {
            reflectionColor = new Color(1f, 0.2f, 0.2f);
        }
        else if (originalColor == new Color(0, 1, 0))
        {
            reflectionColor = new Color(0.75f, 1f, 0.75f);
        }
        else if (originalColor == new Color(0, 0, 1))
        {
            reflectionColor = new Color(0.2f, 0.2f, 1f);
        }
    }

    public void ChangeMaterial(Material mat, Color color)
    {
        sprRend.color = color;
        sprRend.material = mat;
    }

}

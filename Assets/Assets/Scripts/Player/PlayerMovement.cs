﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public delegate void PlayerMovedHandler();
    public event PlayerMovedHandler OnPlayerMoved;
    public event PlayerMovedHandler OnEnemyCancelMovement;

    [Header("Movement Data")]
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float snapDistance = 1f;
    [SerializeField] LayerMask obstacleMask;
    public List<Vector3> tilesToMove = new List<Vector3>();
    [NonSerialized] public Vector3 lastDirection = Vector3.zero;
    public bool moving = false;
    private Vector3 targetPosition;
    public int energy;
    public bool died = false;

    [Header("Material Data")]
    [SerializeField] Material MatEnergy;
    [SerializeField] Material MatNoEnergy;

    [Header("Start Data")]
    private Vector3 originalPos;
    private Quaternion originalRot;
    private int movements = 1;

    [Header("Edit Mode Data")]
    [NonSerialized] public bool placed = true;

    [Header("Components")]
    private Renderer rend;

    private void Awake()
    {
        rend = transform.Find("Model").GetComponent<Renderer>();
        GameManager.i.player = this;
    }

    void Start()
    {
        targetPosition = transform.position;
        originalPos = transform.position;
        originalRot = transform.rotation;
        energy = movements;
    }

    void Update()
    {
        if (EditModeManager.i.playMode && !GameManager.i.paused)
        {
            if (!moving)
            {
                if (energy > 0)
                {
                    if (Input.GetKeyDown(KeyCode.W))
                    {
                        if (TryMove(Vector3.forward))
                        {
                            GameManager.i.player.died = false;
                            UIManager.i.ChangeEnergyUI(false);
                            Invoke("MoveEnemies", 0.2f);
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.S))
                    {
                        if (TryMove(Vector3.back))
                        {
                            GameManager.i.player.died = false;
                            UIManager.i.ChangeEnergyUI(false);
                            Invoke("MoveEnemies", 0.2f);
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.A))
                    {
                        if (TryMove(Vector3.left))
                        {
                            GameManager.i.player.died = false;
                            UIManager.i.ChangeEnergyUI(false);
                            Invoke("MoveEnemies", 0.2f);
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.D))
                    {
                        if (TryMove(Vector3.right))
                        {
                            GameManager.i.player.died = false;
                            UIManager.i.ChangeEnergyUI(false);
                            Invoke("MoveEnemies", 0.2f);
                        }
                    }
                }
                else if (energy == 0)
                {
                    ChangeMaterial(MatNoEnergy);
                }
            }
            else if (moving)
            {
                MoveToDestination();
            }
        }
    }

    private void MoveEnemies()
    {
        OnPlayerMoved?.Invoke();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            died = true;
            UIManager.i.ChangeToEditMode();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Battery"))
        {
            other.transform.parent.gameObject.SetActive(false);
            energy++;
            ChangeMaterial(MatEnergy);
            UIManager.i.ChangeEnergyUI(true);
        }
    }

    public void AvoidEnemyMovement()
    {
        OnEnemyCancelMovement?.Invoke();
    }

    private void MoveToDestination()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(transform.position, targetPosition) < 0.01f)
        {
            transform.position = targetPosition;
            moving = false;

            if (energy == 0)
            {
                ChangeMaterial(MatNoEnergy);
            }
            if (tilesToMove.Count != 0)
            {
                TryMove(tilesToMove[0], false);
            }
        }
    }

    public void Reset()
    {
        transform.position = originalPos;
        transform.rotation = originalRot;
        targetPosition = originalPos;
        lastDirection = Vector3.zero;
        energy = movements;
        tilesToMove.Clear();
        ChangeMaterial(MatEnergy);
        UIManager.i.ChangeEnergyUI(true);
    }

    public bool TryMove(Vector3 direction, bool isInput = true)
    {
        if (moving)
        {
            return false;
        }

        lastDirection = direction;
        if (tilesToMove.Count != 0)
        {
            tilesToMove.RemoveAt(0);
        }

        if (!Physics.Raycast(transform.position, direction, snapDistance, obstacleMask))
        {
            // Move and rotate to the potential position
            targetPosition = transform.position + direction * snapDistance;
            RotatePlayer(direction);
            moving = true;
            if (isInput)
            {
                energy--;
            }
            return true;
        }

        // Debug the raycast
        Debug.DrawRay(transform.position, direction * snapDistance, Color.red, 1f);
        return false;
    }

    private void RotatePlayer(Vector3 direction)
    {
        if (direction == Vector3.forward)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.left);
        }
        else if (direction == Vector3.back)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.right);
        }
        else if (direction == Vector3.left)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.back);
        }
        else if (direction == Vector3.right)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward);
        }
    }

    private void ChangeMaterial(Material mat)
    {
        rend.material = mat;
    }
}

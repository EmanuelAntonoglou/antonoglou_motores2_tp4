using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileController : MonoBehaviour
{
    [Header("Tile Data")]
    public bool canEdit = true;
    [NonSerialized] public bool taken;

    [Header("Hover Data")]
    [SerializeField] private Color normalColor;
    [SerializeField] private Color placedColor;
    [SerializeField] private Color takenColor;
    [SerializeField] private float normalThickness;
    [SerializeField] private float hoverThickness;

    [Header("Components")]
    private Renderer rend;
    private MaterialPropertyBlock propertyBlock;
    private Material originalMat;


    private void Awake()
    {
        rend = GetComponent<Renderer>();
        propertyBlock = new MaterialPropertyBlock();
    }

    private void Start()
    {
        MaterialController.i.OnAddGridMaterial += AddGridToTile;
        taken = !canEdit;
        if (!canEdit) { ChangeMaterialProperties(1, takenColor, normalThickness); }

        if (rend != null && rend.materials.Length > 1)
        {
            originalMat = rend.materials[1];
        }
    }

    private void OnDestroy()
    {
        MaterialController.i.OnAddGridMaterial -= AddGridToTile;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Placeable" || other.gameObject.tag == "Tile")
        {
            DetectIfCanPlaceEnter(other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            DetectIfCanPlaceEnter(collision.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pointer"))
        {
            if (!taken)
            {
                ChangeMaterialProperties(0, placedColor, hoverThickness);
            }
            else
            {
                ChangeMaterialProperties(0, takenColor, hoverThickness);
            }
        }
    }

    public void  OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pointer"))
        {
            if (!taken)
            {
                ChangeMaterialProperties(0, normalColor, normalThickness);
            }
            else
            {
                ChangeMaterialProperties(0, takenColor, normalThickness);
            }
        }
    }

    private void DetectIfCanPlaceEnter(GameObject GO)
    {
        if (!EditModeManager.i.playMode)
        {
            PlaceableObject placeableOBJ = GO.transform.GetComponent<PlaceableObject>();
            PlayerMovement player = GO.transform.GetComponent<PlayerMovement>();

            if ((placeableOBJ != null && placeableOBJ.placed) ||
                (player != null && player.placed))
            {
                taken = true;
            }
            //else if (placeableOBJ != null && taken && !placeableOBJ.placed)
            //{
            //    EditModeManager.i.canEdit = false;
            //}
            //else
            //{
            //    EditModeManager.i.canEdit = true;
            //}
        }
    }

    public void PlaceTile()
    {
        ChangeMaterialProperties(0, takenColor, hoverThickness);
        taken = true;
    }

    public void DeleteTile()
    {
        SetMainMat();
        ChangeMaterialProperties(1, normalColor, normalThickness);
        taken = false;
    }

    public void DeleteMainMat()
    {
        if (rend != null && rend.materials.Length > 1)
        {
            Material[] newMaterials = new Material[rend.materials.Length - 1];
            newMaterials[0] = rend.materials[0];
            for (int i = 2; i < rend.materials.Length; i++)
            {
                newMaterials[i - 1] = rend.materials[i];
            }
            rend.materials = newMaterials;
        }
    }

    public void SetMainMat()
    {
        if (originalMat != null)
        {
            if (rend != null)
            {
                Material[] newMaterials = new Material[rend.materials.Length + 1];
                newMaterials[0] = rend.materials[0];
                newMaterials[1] = originalMat;
                for (int i = 1; i < rend.materials.Length; i++)
                {
                    newMaterials[i + 1] = rend.materials[i];
                }
                rend.materials = newMaterials;
            }
        }
    }

    public void AddGridToTile(bool add)
    {
        if (add)
        {
            AddMaterial(0, MaterialController.i.mat1);
            if (taken)
            {
                ChangeMaterialProperties(0, takenColor, normalThickness);
            }
            else
            {
                ChangeMaterialProperties(0, normalColor, normalThickness);
            }
        }
        else
        {
            RemoveMaterial(0);
        }
    }

    public void AddMaterial(int pos, Material newMat)
    {
        if (rend != null)
        {
            Material[] currentMaterials = rend.materials;
            Material[] newMaterials = new Material[currentMaterials.Length + 1];

            for (int i = 0, j = 0; i < newMaterials.Length; i++)
            {
                if (i == pos)
                {
                    newMaterials[i] = newMat;
                }
                else
                {
                    newMaterials[i] = currentMaterials[j];
                    j++;
                }
            }
            rend.materials = newMaterials;
        }
    }

    public void RemoveMaterial(int index)
    {
        if (rend != null)
        {
            Material[] currentMaterials = rend.materials;
            List<Material> newMaterials = new List<Material>(currentMaterials);

            newMaterials.RemoveAt(index);
            rend.materials = newMaterials.ToArray();
        }
    }

    public void ChangeMaterialProperties(int index, Color color, float thickness)
    {
        if (rend != null)
        {
            Material[] currentMaterials = rend.materials;
            if (index >= 0 && index < currentMaterials.Length)
            {
                // Obtener propiedades actuales
                rend.GetPropertyBlock(propertyBlock, index);

                // Cambiar propiedades
                propertyBlock.SetColor("_Color", color);
                propertyBlock.SetFloat("_Thickness", thickness);

                // Aplicar propiedades modificadas
                rend.SetPropertyBlock(propertyBlock, index);
            }
        }
    }


}

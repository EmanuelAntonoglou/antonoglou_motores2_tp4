using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EditModeManager : MonoBehaviour
{
    [NonSerialized] public static EditModeManager i;

    public delegate void EditModeChangedHandler();
    public event EditModeChangedHandler OnEditModeChanged;

    public delegate void EditPlayChangedHandler();
    public event EditPlayChangedHandler OnPlayModeChanged;

    [Header("Edit Mode Data")]
    [NonSerialized] public bool playMode = false;
    [NonSerialized] public bool canPlace = false;

    [Header("Snap Data")]
    [SerializeField] private float snapValue;
    [SerializeField] private Vector3 snapOffset;
    [SerializeField] private float ZDepth;

    [Header("Instantiator Data")]
    [SerializeField] private GameObject instances;
    [SerializeField] private GameObject prefab1;
    [SerializeField] private GameObject prefab2;
    [NonSerialized] public GameObject currentInstance;
    private int lastIndex = -1;

    [Header("UI Data")]
    [NonSerialized] public int pressedBTN = -1;

    //Edit Mode Data
    public delegate void ResetEditState();
    public static event ResetEditState OnResetEditState;

    private void Awake()
    {
        CrearSingleton();
    }

    private void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (!playMode)
        {
            //InstantiatorHandlerByKeyboard();
            if (currentInstance != null && !GameManager.i.paused)
            {
                MoveInstanceWithMouse();

                if (Input.GetMouseButton(0) && canPlace)
                {
                    PlaceInstance();
                }
            }
        }
    }

    public void EditModeChanged()
    {
        OnEditModeChanged?.Invoke();
    }

    public void PlayModeChanged()
    {
        OnPlayModeChanged?.Invoke();
    }

    public void InstantiatorHandler(GameObject prefab)
    {
        if (currentInstance != null)
        {
            PlaceableObject placeableOBJ = currentInstance.GetComponent<PlaceableObject>();
            if (pressedBTN == lastIndex)
            {
                placeableOBJ.DestroyPlaceableObject();
                
            }
            else
            {
                if (pressedBTN != -1)
                {
                    placeableOBJ.DestroyPlaceableObject();
                    CreateInstance(prefab);
                }
            }
        }
        else
        {
            if (pressedBTN != -1)
            {
                CreateInstance(prefab);
            }
        }
        lastIndex = pressedBTN;
    }

    private void InstantiatorHandlerByKeyboard()
    {
        int index = GetIndexByKeyboard();
        if (currentInstance != null)
        {
            PlaceableObject placeableOBJ = currentInstance.GetComponent<PlaceableObject>();
            if (index == lastIndex)
            {
                placeableOBJ.DestroyPlaceableObject();
                lastIndex = -1;
            }
            else
            {
                if (index != -1)
                {
                    placeableOBJ.DestroyPlaceableObject();
                    CreateInstance(GetPrefabByIndex(index));
                }
            }
        }
        else
        {
            if (index != -1)
            {
                CreateInstance(GetPrefabByIndex(index));
            }
        }
    }

    private int GetIndexByKeyboard()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            return 0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            return 1;
        }
        return -1;
    }

    private GameObject GetPrefabByIndex(int i)
    {
        lastIndex = i;
        if (i == 0)
        {
            return prefab1;
        }
        else if (i == 1)
        {
            return prefab2;
        }
        return null;
    }

    private void CreateInstance(GameObject prefab)
    {
        currentInstance = Instantiate(prefab);
        currentInstance.transform.SetParent(instances.transform);
        currentInstance.transform.position = Input.mousePosition;
        currentInstance.GetComponent<PlaceableObject>().originalPos = Input.mousePosition;
    }

    public void DeleteAllInstances()
    {
        foreach (Transform child in instances.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void MoveInstanceWithMouse()
    {
        if (currentInstance != null)
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = ZDepth;
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

            //Snap
            float snappedX = Mathf.Round(worldPosition.x / snapValue) * snapValue;
            float snappedZ = Mathf.Round(worldPosition.z / snapValue) * snapValue;

            Vector3 finalPos = new Vector3(snappedX + snapOffset.x, snapOffset.y, snappedZ + snapOffset.z);
            currentInstance.transform.position = finalPos;
        }
    }

    private void PlaceInstance()
    {
        PlaceableObject placeableOBJ = currentInstance.GetComponent<PlaceableObject>();

        placeableOBJ.placed = true;
        placeableOBJ.originalPos = placeableOBJ.transform.position;
        placeableOBJ.tile.DeleteMainMat();
        placeableOBJ.tile.PlaceTile();

        canPlace = false;
        currentInstance = null;

        ChangeTileUI();
        CheckForNextTile();
    }

    private void ChangeTileUI()
    {
        LevelData.TileData tileData = GameManager.i.levelData.ReturnTileData(pressedBTN);
        tileData.actualAmount -= 1;
        GameManager.i.levelData.levelTiles[pressedBTN] = tileData;

        Transform pressedButtonTransform = UIManager.i.horGroup.transform.GetChild(pressedBTN);
        TextMeshProUGUI amountText = pressedButtonTransform.transform.Find("OBJ_AmountBox").Find("TMP_Amount").GetComponentInChildren<TextMeshProUGUI>();
        amountText.text = tileData.actualAmount.ToString();
    }

    public void ChangeAmountUI(int i)
    {
        if (UIManager.i.horGroup != null)
        {
            Transform HLGTransform = UIManager.i.horGroup?.transform;
            Transform tileBTN = HLGTransform.GetChild(i);

            if (GameManager.i.levelData.levelTiles[i].actualAmount != 0)
            {
                Image tileIMG = tileBTN.Find("OBJ_AmountBox").GetComponent<Image>();
                UIManager.i.UpdateTileUIColors(tileIMG, true);
            }
            else
            {
                Image tileIMG = tileBTN.Find("OBJ_AmountBox").GetComponent<Image>();
                UIManager.i.UpdateTileUIColors(tileIMG, false);
            }

            LevelData.TileData tileData = GameManager.i.levelData.levelTiles[i];
            GameManager.i.levelData.levelTiles[i] = tileData;
            TextMeshProUGUI tileAmountTXT = tileBTN.Find("OBJ_AmountBox").Find("TMP_Amount").GetComponent<TextMeshProUGUI>();
            tileAmountTXT.text = GameManager.i.levelData.levelTiles[i].actualAmount.ToString();
        }
    }

    private void CheckForNextTile()
    {
        if (GameManager.i.levelData.levelTiles[pressedBTN].actualAmount > 0)
        {
            CreateInstance(GameManager.i.levelData.ReturnTileData(pressedBTN).tilePrefab);
        }
        else
        {
            UIManager.i.UpdateTileUIColors(UIManager.i.activeToggle, false);
            UIManager.i.UpdateTileUIColors(UIManager.i.activeToggleImg, false);
            UIManager.i.activeToggle.isOn = false;
            UIManager.i.activeToggle = null;
            pressedBTN = -1;
        }
    }

    public void ChangeToOriginalStateForAll()
    {
        OnResetEditState?.Invoke();
    }

}

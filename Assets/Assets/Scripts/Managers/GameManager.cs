using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager i;

    public PointerController pointer;

    [Header("Level Data")]
    public int level = 1;
    public LevelData levelData;
    [NonSerialized] public PlayerMovement player;
    [NonSerialized] public bool paused = false;
    [NonSerialized] public bool lever = false;
    [NonSerialized] public LeverController leverCon;

    [Header("Tutorial Conditions")]
    [NonSerialized] public bool hasChangeToPlayMode = false;
    [NonSerialized] public bool hasChangeToEditMode = false;
    [NonSerialized] public bool hasChangeToEditModeFlag = false;

    private void Awake()
    {
        CreateSingleton();
    }

    private void Start()
    {
        ChangeLevel(level);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                UIManager.i.OnPauseClicked();
            }
            else
            {
                UIManager.i.CNV_Pause.OnResumeClicked();
            }
        }
    }

    private void CreateSingleton()
    {
        if (i == null)
        {
            i = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeLevel(int pLevel)
    {
        StartCoroutine(ChangeLevelCoroutine(pLevel));

        if (level == 1)
        {
            hasChangeToPlayMode = false;
            hasChangeToEditMode = false;
        }
    }

    public void ChangeToNextLevel()
    {
        if (SceneExists("Level " + (level + 1)))
        {
            ChangeLevel(level + 1);
        }
        else
        {
            Debug.Log("No hay m�s niveles disponibles.");
        }
    }

    private IEnumerator ChangeLevelCoroutine(int pLevel)
    {
        string sceneToUnload = "Level " + level;
        if (SceneManager.GetSceneByName(sceneToUnload).isLoaded)
        {
            yield return SceneManager.UnloadSceneAsync(sceneToUnload);
        }

        level = pLevel;
        LoadSceneOverCurrent("Level " + level);
    }

    public void LoadSceneOverCurrent(string sceneName)
    {
        EditModeManager.i.DeleteAllInstances();
        UIManager.i.ChangeLevel(level);
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
    }

    public void UnloadScene(string sceneName)
    {
        if (SceneManager.GetSceneByName(sceneName).isLoaded)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }
    }

    public void Reset()
    {
        if (player != null)
        {
            player.Reset();
        }
        EditModeManager.i.ChangeToOriginalStateForAll();
        EditModeManager.i.EditModeChanged();
    }

    private bool SceneExists(string sceneName)
    {
        int sceneIndex = SceneUtility.GetBuildIndexByScenePath(sceneName);
        return sceneIndex != -1;
    }
}

using System;
using Unity.VisualScripting;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public static TurnManager i { get; private set; }

    [Header("Turn Data")]
    [SerializeField] private float turnDuration;
    private int turnCounter = 0;


    private void Awake()
    {
        CreateSingleton();
    }

    private void CreateSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void StartTurn()
    {
        InvokeRepeating("AdvanceTurn", turnDuration, 2);
    }

    //private void AdvanceTurn()
    //{
    //    turnCounter++;
    //    Vector3 playerPos = GameManager.i.player.transform.position;
    //    if (IsRedTurn())
    //    {
    //        UIManager.i.ChangeTurn(turnCounter, true);
    //    }
    //    else
    //    {
    //        UIManager.i.ChangeTurn(turnCounter, false);
    //    }
    //}

    //public void StopTurn()
    //{
    //    CancelInvoke("AdvanceTurn");
    //}

    public void ResetTurn()
    {
        turnCounter = 0;
    }

    public bool IsRedTurn()
    {
        return turnCounter % 2 != 0;
    }

    public bool IsBlueTurn()
    {
        return turnCounter % 2 == 0;
    }

    public void NextTurn()
    {
        turnCounter++;
    }
}

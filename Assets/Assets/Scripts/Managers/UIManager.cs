using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [NonSerialized] public static UIManager i;

    [Header("UI References")]
    [SerializeField] private Color turnRedColor;
    [SerializeField] private Color turnBlueColor;
    [SerializeField] public GameObject playModeGO;
    [SerializeField] public GameObject editModeGO;
    [SerializeField] public Image energyUI;

    [Header("UI Tile References")]
    public Toggle activeToggle;
    public Image activeToggleImg;

    [Header("Level References")]
    [SerializeField] public TextMeshProUGUI TMPLevel;
    [SerializeField] public Button BTNPlay;

    [Header("Canvas References")]
    public GameObject horGroup;
    public PauseMenu CNV_Pause;
    public GameObject BTN_Pause;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void OnPauseClicked()
    {
        CNV_Pause.gameObject.SetActive(true);
        BTN_Pause.SetActive(false);
        Time.timeScale = 0f;
    }

    public void ChangeLevel(int level)
    {
        TMPLevel.text = "Level " + level;
        ChangeToEditMode();
    }

    public void ChangeToPlayMode()
    {
        if (GameManager.i.level == 1)
        {
            GameManager.i.hasChangeToPlayMode = true;
        }

        EditModeManager.i.PlayModeChanged();
        ChangeAllTileUIColors();
        MaterialController.i.AddTilesGridMaterial(false);
        EditModeManager.i.playMode = true;
        playModeGO.SetActive(false);
        editModeGO.SetActive(true);
        horGroup.SetActive(false);
        DeselectAllTileUI();
        Destroy(EditModeManager.i.currentInstance);
    }

    public void ChangeToEditMode()
    {
        if (GameManager.i.level == 1 && GameManager.i.hasChangeToEditModeFlag)
        {
            GameManager.i.hasChangeToEditMode = true;
        }
        GameManager.i.hasChangeToEditModeFlag = true;

        MaterialController.i.AddTilesGridMaterial(true);
        EditModeManager.i.playMode = false;
        editModeGO.SetActive(false);
        playModeGO.SetActive(true);
        GameManager.i.Reset();
        if (GameManager.i.player != null)
        {
            GameManager.i.player.AvoidEnemyMovement();
        }
        horGroup.SetActive(true);
    }

    public void OnTileBTNPressed(Toggle toggle, int index)
    {
        if (GameManager.i.levelData.levelTiles[index].actualAmount > 0)
        {
            if (activeToggle == null)
            {
                SelectTileUI(toggle, index);
            }
            else if (activeToggle != null && activeToggle != toggle)
            {
                activeToggle.isOn = false;
                SelectTileUI(toggle, index);
            }
            else if (activeToggle != null && activeToggle == toggle)
            {
                DeselectTileUI(toggle);
                Destroy(EditModeManager.i.currentInstance); 
            }
            EditModeManager.i.InstantiatorHandler(GameManager.i.levelData.levelTiles[index].tilePrefab);
        }
    }

    private void SelectTileUI(Toggle toggle, int index)
    {
        UpdateTileUIColors(toggle, true);
        activeToggle = toggle;
        activeToggleImg = activeToggle.transform.parent.Find("OBJ_AmountBox").GetComponent<Image>();
        EditModeManager.i.pressedBTN = index;
    }

    private void DeselectTileUI(Toggle toggle)
    {
        if (activeToggle != null && toggle != null)
        {
            activeToggle.isOn = false;
            UpdateTileUIColors(toggle, false);
            UpdateTileUIColors(activeToggle, false);
        }
        activeToggle = null;
        EditModeManager.i.pressedBTN = -1;
    }

    private void DeselectAllTileUI()
    {
        foreach (Transform tileUI in horGroup.transform)
        {
            DeselectTileUI(tileUI.GetComponent<Toggle>());
        }
    }

    private void ChangeAllTileUIColors()
    {
        foreach (Transform tileUI in horGroup.transform)
        {
            UpdateTileUIColors(tileUI.GetComponentInChildren<Toggle>(), false);
        }
    }

    public void UpdateTileUIColors(Toggle toggle, bool isOn)
    {
        if (isOn)
        {
            toggle.GetComponent<Image>().color = new Color(83f / 255f, 129f / 255f, 255f / 255f, 1f); // Azul
        }
        else
        {
            toggle.GetComponent<Image>().color = Color.white;
        }
    }

    public void UpdateTileUIColors(Image img, bool isOn)
    {
        if (isOn)
        {
            img.GetComponent<Image>().color = new Color(24f / 255f, 224f / 255f, 0f / 255f, 1f); // Verde
        }
        else
        {
            img.GetComponent<Image>().color = new Color(224f / 255f, 38f / 255f, 0f / 255f, 1f); // Rojo
        }
    }

    public void ChangeEnergyUI(bool energy)
    {
        if (energyUI)
        {
            if (energy)
            {
                energyUI.color = new Color(250f / 255f, 205f / 255f, 10f / 255f, 1f); // Amarillo
            }
            else
            {
                energyUI.color = new Color(225f / 255f, 25f / 255f, 10f / 255f, 1f); // Rojo
            }
        }
    }

}

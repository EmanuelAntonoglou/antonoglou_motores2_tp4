using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> tutorials;
    private int index = 0;
    public bool checkForCondition = false;

    private void Update()
    {
        if (checkForCondition)
        {
            IsConditionMet();
        }
    }

    public void IsConditionMet()
    {
        if (index == 0 && GameManager.i.hasChangeToPlayMode)
        {
            NextTutorial();
        }
        else if (index == 1 && EditModeManager.i.playMode &&
                (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D)))
        {
            NextTutorial();
        }
        else if (index == 2 && GameManager.i.hasChangeToEditMode)
        {
            NextTutorial();
        }
    }

    public void NextTutorial()
    {
        index++;
        tutorials[index].SetActive(true);
        checkForCondition = false;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Unity.VisualScripting;
using UnityEngine;

[Serializable]
public class LevelData
{
    public int energy = 0;
    public List<TileData> levelTiles;

    [Serializable] public struct TileData
    {
        public GameObject tilePrefab;
        public int amount;
        [NonSerialized] public int actualAmount;
    }

    public void ResetLevelData()
    {
        ResetPlayerEnergy();
        SetActualAmount();
    }

    public void ResetPlayerEnergy()
    {
        GameManager.i.player.energy = energy;
    }

    public TileData ReturnTileData(int index)
    {
        return GameManager.i.levelData.levelTiles[index];
    }

    public void SetActualAmount()
    {
        for (int i = 0; i < levelTiles.Count; i++)
        {
            TileData tile = levelTiles[i];
            tile.actualAmount = tile.amount;
            levelTiles[i] = tile;
        }
    }

    public void SetTileIndex()
    {
        // Un diccionario para rastrear el �ndice actual de cada tipo derivado de Tile
        Dictionary<Type, int> typeIndexMap = new Dictionary<Type, int>();

        for (int i = 0; i < levelTiles.Count; i++)
        {
            TileData tileData = levelTiles[i];

            // Obtener el tipo del componente Tile
            Type tileType = tileData.tilePrefab.GetComponent<Tile>().GetType();

            // Inicializar el �ndice para este tipo si a�n no est� en el diccionario
            if (!typeIndexMap.ContainsKey(tileType))
            {
                typeIndexMap[tileType] = 0;
            }

            // Obtener el campo est�tico index de este tipo
            FieldInfo indexField = tileType.GetField("index", BindingFlags.Static | BindingFlags.Public);

            if (indexField != null && indexField.FieldType == typeof(int))
            {
                // Asignar el �ndice del bucle for al campo est�tico index
                indexField.SetValue(null, i);
                // Incrementar el �ndice para este tipo
                typeIndexMap[tileType]++;
            }
        }
    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelHandler : MonoBehaviour
{
    [SerializeField] private GameObject BTN_Tile;
    [SerializeField] private Vector3 pointerOffset = new Vector3(0.5f, 1f, 0.5f);
    [SerializeField] private LevelData levelData;

    [Header("UI Data")]
    private GameObject horGroup;

    private void Awake()
    {
        horGroup = UIManager.i.horGroup;
    }

    private void Start()
    {
        GameManager.i.levelData = levelData;
        GameManager.i.pointer.snapOffset = pointerOffset;
        DestroyUI();
        CreateUI();
        levelData.SetActualAmount();
        levelData.SetTileIndex();
    }

    private void CreateUI()
    {
        for (int i = 0; i < levelData.levelTiles.Count; i++)
        {
            GameObject BTN = Instantiate(BTN_Tile);
            GameObject BTN_Amount = BTN.transform.Find("OBJ_AmountBox").gameObject;
            TextMeshProUGUI TXT_Amount = BTN_Amount.transform.Find("TMP_Amount").GetComponent<TextMeshProUGUI>();
            Toggle TGL_Icon = BTN.transform.Find("TGL_IconBox").GetComponent<Toggle>();
            Image IMG_Icon = TGL_Icon.transform.Find("SPR_Icon").GetComponent<Image>();

            BTN.transform.SetParent(horGroup.transform);
            BTN.transform.localScale = Vector3.one;

            int index = i;
            TGL_Icon.onValueChanged.AddListener((bool isOn) => UIManager.i.OnTileBTNPressed(TGL_Icon, index));
            TXT_Amount.text = levelData.levelTiles[i].amount.ToString();
            IMG_Icon.sprite = levelData.levelTiles[i].tilePrefab.GetComponent<Tile>()?.spr;
        }
    }

    private void DestroyUI()
    {
        foreach (Transform btn in horGroup.transform)
        {
            Destroy(btn.gameObject);
        }
    }

}

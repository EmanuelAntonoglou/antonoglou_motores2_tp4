using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TutorialData
{
    public Vector2 focusPosition;
    public Vector3 boxPosition;
    [TextArea(2, 10)] public string message;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public void OnResumeClicked()
    {
        Resume();
    }

    public void OnRestartClicked()
    {
        Resume();
        GameManager.i.ChangeLevel(GameManager.i.level);
    }

    public void OnQuitClicked()
    {
        Application.Quit();
    }

    private void Resume()
    {
        gameObject.SetActive(false);
        UIManager.i.BTN_Pause.SetActive(true);
        Time.timeScale = 1f;
        GameManager.i.paused = false;
    }

}

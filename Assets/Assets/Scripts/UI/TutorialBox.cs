using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialBox : MonoBehaviour
{
    public TextMeshProUGUI TMP_Message;
    public TextMeshProUGUI TMP_ClickContinue;
    public float speed;
    public float holdTime;


    private void Start()
    {
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        Color originalColor = TMP_ClickContinue.color;

        while (true)
        {
            float elapsed = 0f;

            TMP_ClickContinue.color = new Color(originalColor.r, originalColor.g, originalColor.b, 1f);
            yield return new WaitForSeconds(holdTime);

            while (elapsed < speed)
            {
                elapsed += Time.deltaTime;
                float alpha = Mathf.Lerp(1f, 0f, elapsed / speed);
                TMP_ClickContinue.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
                yield return null;
            }

            elapsed = 0f;

            while (elapsed < speed)
            {
                elapsed += Time.deltaTime;
                float alpha = Mathf.Lerp(0f, 1f, elapsed / speed);
                TMP_ClickContinue.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
                yield return null;
            }
        }
    }

}

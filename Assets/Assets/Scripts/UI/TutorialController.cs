using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class TutorialController : MonoBehaviour
{
    private TutorialManager tutorialManager;
    [SerializeField] private float focusSpeed;
    [SerializeField] private float focusMoveSpeed;
    [SerializeField] private float fadeInDuration;
    [SerializeField] private GameObject tutorialBox;
    [SerializeField] private List<TutorialData> tutorialSequence = new List<TutorialData>();
    private GameObject canvas;
    private Volume volume;
    private Vignette vignette;
    private GameObject tutorialInstance;
    private int index = -1;
    private bool canInteract = false;
    private Vector2 lastFocusPos;

    private void Awake()
    {
        volume = GetComponent<Volume>();
        tutorialManager = transform.parent.parent.GetComponent<TutorialManager>();
    }

    void Start()
    {
        canvas = transform.parent.gameObject;
        if (volume != null && volume.profile.TryGet<Vignette>(out vignette))
        {
            vignette.intensity.value = 0f;
            SetCenter(tutorialSequence[0].focusPosition);
            lastFocusPos = tutorialSequence[0].focusPosition;
            StartCoroutine(IncreaseFocusIntensity(focusSpeed));
        }
    }

    private void Update()
    {
        if (canInteract && Input.GetMouseButtonDown(0))
        {
            StartCoroutine(FadeOuttBox());
        }
    }

    public void SetIntensity(float intensity)
    {
        if (vignette != null)
        {
            vignette.intensity.value = intensity;
        }
    }

    public void SetCenter(Vector2 center)
    {
        if (vignette != null)
        {
            vignette.center.value = center;
        }
    }

    private IEnumerator MoveToCenter(float duration, Vector2 startPos, Vector2 finishPos)
    {
        float startTime = Time.time;

        while (Time.time < startTime + duration)
        {
            float elapsed = Time.time - startTime;
            float centerX = Mathf.Lerp(startPos.x, finishPos.x, elapsed / duration);
            float centerY = Mathf.Lerp(startPos.y, finishPos.y, elapsed / duration);
            Vector2 center = new Vector2(centerX, centerY);
            SetCenter(center);

            yield return null;
        }

        SetCenter(finishPos);
        lastFocusPos = finishPos;
        if (index < tutorialSequence.Count)
        {
            CreateTutorialBox();
        }
        else
        {
            StartCoroutine(DecreaseFocusIntensity(focusSpeed));
        }
    }


    private IEnumerator IncreaseFocusIntensity(float duration)
    {
        float startTime = Time.time;

        while (Time.time < startTime + duration)
        {
            float elapsed = Time.time - startTime;
            float smoothness = Mathf.Lerp(0f, 1f, elapsed / duration);
            SetIntensity(smoothness);

            yield return null;
        }
        NextBox();
    }

    private IEnumerator DecreaseFocusIntensity(float duration)
    {
        float startTime = Time.time;

        while (Time.time < startTime + duration)
        {
            float elapsed = Time.time - startTime;
            float smoothness = Mathf.Lerp(1f, 0f, elapsed / duration);
            SetIntensity(smoothness);

            yield return null;
        }
        tutorialManager.checkForCondition = true;
        SetIntensity(0f);
        Destroy(canvas);
    }

    private void CreateTutorialBox()
    {
        tutorialInstance = Instantiate(tutorialBox);
        tutorialInstance.transform.SetParent(canvas.transform, false);
        tutorialInstance.GetComponent<RectTransform>().anchoredPosition = tutorialSequence[index].boxPosition;
        tutorialInstance.GetComponent<TutorialBox>().TMP_Message.text = tutorialSequence[index].message;

        CanvasGroup canvasGroup = tutorialInstance.GetComponent<CanvasGroup>();
        if (canvasGroup == null)
        {
            canvasGroup = tutorialInstance.AddComponent<CanvasGroup>();
        }
        canvasGroup.alpha = 0f;
        StartCoroutine(FadeInBox(canvasGroup, fadeInDuration));
    }

    private void NextBox()
    {
        index++;

        if (index < tutorialSequence.Count)
        {
            if (tutorialSequence[index].focusPosition != lastFocusPos)
            {
                StartCoroutine(MoveToCenter(focusMoveSpeed, lastFocusPos, tutorialSequence[index].focusPosition));
            }
            else
            {
                CreateTutorialBox();
            }
        }
        else
        {
            StartCoroutine(DecreaseFocusIntensity(focusSpeed));
        }
    }

    private IEnumerator FadeInBox(CanvasGroup canvasGroup, float duration)
    {
        float startTime = Time.time;

        while (Time.time < startTime + duration)
        {
            float elapsed = Time.time - startTime;
            if (canvasGroup != null)
            {
                canvasGroup.alpha = Mathf.Lerp(0f, 1f, elapsed / duration);
            }
            yield return null;
        }
        if (canvasGroup != null)
        {
            canvasGroup.alpha = 1f;
        }
        canInteract = true;
    }

    private IEnumerator FadeOuttBox()
    {
        if (tutorialInstance != null)
        {
            CanvasGroup canvasGroup = tutorialInstance.GetComponent<CanvasGroup>();
            float startTime = Time.time;
            canInteract = false;

            while (Time.time < startTime + fadeInDuration)
            {
                float elapsed = Time.time - startTime;
                if (canvasGroup != null)
                {
                    canvasGroup.alpha = Mathf.Lerp(1f, 0f, elapsed / fadeInDuration);
                }
                yield return null;
            }
            if (canvasGroup != null)
            {
                canvasGroup.alpha = 0f;
            }
            Destroy(tutorialInstance);
        }
        NextBox();
    }

}

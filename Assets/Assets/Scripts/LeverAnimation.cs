using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverAnimation : MonoBehaviour
{
    public void OnLeverUp()
    {
        transform.parent.parent.GetComponent<LeverController>().LeverUp();
    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Tile : MonoBehaviour
{
    [Header("Edit Mode Data")]
    public bool canEdit = true;
    public bool taken = false;

    [Header("Hover Data")]
    [SerializeField] private Color normalColor;
    [SerializeField] private Color placedColor;
    [SerializeField] private Color takenColor;
    [SerializeField] private float normalThickness;
    [SerializeField] private float hoverThickness;

    [Header("UI Data")]
    [SerializeField] public Sprite spr;

    [Header("Components")]
    private Renderer rend;
    private MaterialPropertyBlock propertyBlock;


    private void Awake()
    {
        rend = GetComponent<Renderer>();
        propertyBlock = new MaterialPropertyBlock();
    }

    private void OnTriggerEnter(Collider other)
    {
        CheckTriggerEnter(other);
    }

    protected virtual void CheckTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pointer"))
        {
            if (!taken)
            {
                ChangeMaterialProperties(1, placedColor, hoverThickness);
            }
            else
            {
                ChangeMaterialProperties(1, takenColor, hoverThickness);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pointer"))
        {
            if (!taken)
            {
                ChangeMaterialProperties(1, normalColor, normalThickness);
            }
            else
            {
                ChangeMaterialProperties(1, takenColor, normalThickness);
            }
        }
    }

    public void PlaceTile()
    {
        ChangeMaterialProperties(1, takenColor, hoverThickness);
    }

    public void ChangeMaterialProperties(int index, Color color, float thickness)
    {
        if (rend != null)
        {
            Material[] currentMaterials = rend.materials;
            if (index >= 0 && index < currentMaterials.Length)
            {
                // Obtener propiedades actuales
                rend.GetPropertyBlock(propertyBlock, index);

                // Cambiar propiedades
                propertyBlock.SetColor("_Color", color);
                propertyBlock.SetFloat("_Thickness", thickness);

                // Aplicar propiedades modificadas
                rend.SetPropertyBlock(propertyBlock, index);
            }
        }
    }

}

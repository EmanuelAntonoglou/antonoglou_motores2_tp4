using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TGoal : Tile
{
    private void OnTriggerEnter(Collider other)
    {
        base.CheckTriggerEnter(other);
        if (other.CompareTag("Player"))
        {
            UIManager.i.ChangeEnergyUI(true);
            GameManager.i.ChangeToNextLevel();
        }
    }
}

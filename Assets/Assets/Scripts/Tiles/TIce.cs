using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TIce : Tile
{
    public static int index = 0;

    protected void OnDestroy()
    {
        EditModeManager.i.ChangeAmountUI(index);
    }

    private void OnTriggerEnter(Collider other)
    {
        base.CheckTriggerEnter(other);
        if (EditModeManager.i.playMode)
        {
            if (other.CompareTag("Player"))
            {
                PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
                playerMovement?.tilesToMove.Add(playerMovement.lastDirection);
            }
            if (other.CompareTag("Enemy"))
            {
                EnemyController enemy = other.GetComponent<EnemyController>();
                enemy.tilesToMove.Add(enemy.tilesToMove[enemy.tilesToMove.Count-1]);
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSwitch : Tile
{
    public static int index = 0;

    [NonSerialized] private Quaternion originalRot;

    void Start()
    {
        EditModeManager.i.OnPlayModeChanged += SetOriginalRot;
        EditModeManager.i.OnEditModeChanged += ChangeToOriginalRot;
        GameManager.i.leverCon.LeverUpChanged += InvertRot;
    }

    private void OnDestroy()
    {
        EditModeManager.i.OnPlayModeChanged -= SetOriginalRot;
        EditModeManager.i.OnEditModeChanged -= ChangeToOriginalRot;
        GameManager.i.leverCon.LeverUpChanged -= InvertRot;
        EditModeManager.i.ChangeAmountUI(index);
    }

    private void OnTriggerEnter(Collider other)
    {
        base.CheckTriggerEnter(other);
        if (EditModeManager.i.playMode)
        {
            if (other.CompareTag("Player"))
            {
                PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
                playerMovement?.tilesToMove.Add(transform.forward);
            }
            if (other.CompareTag("Enemy"))
            {
                EnemyController enemy = other.GetComponent<EnemyController>();
                enemy.tilesToMove.Add(transform.forward);
            }
        }
    }
    public void SetOriginalRot()
    {
        originalRot = transform.localRotation;
    }

    public void ChangeToOriginalRot()
    {
        transform.localRotation = originalRot;
    }

    public void InvertRot()
    {
        transform.localRotation *= Quaternion.Euler(0, 180, 0);
    }
}

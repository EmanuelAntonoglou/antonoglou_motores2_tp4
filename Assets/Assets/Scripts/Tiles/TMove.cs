using UnityEngine;

public class TMove : Tile
{
    public static int index = 0;

    protected void OnDestroy()
    {
        EditModeManager.i.ChangeAmountUI(index);
    }

    private void OnTriggerEnter(Collider other)
    {
        base.CheckTriggerEnter(other);
        if (EditModeManager.i.playMode)
        {
            if (other.CompareTag("Player"))
            {
                PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
                playerMovement?.tilesToMove.Add(transform.forward);
            }
            if (other.CompareTag("Enemy"))
            {
                EnemyController enemy = other.GetComponent<EnemyController>();
                enemy.tilesToMove.Add(transform.forward);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverController : MonoBehaviour
{
    public delegate void LeverUpHandler();
    public event LeverUpHandler LeverUpChanged;

    [SerializeField] private Animator animator;
    private bool pointerOn = false;
    private bool active = false;

    private void Awake()
    {
        GameManager.i.leverCon = this;
    }

    private void Start()
    {
        EditModeManager.i.OnEditModeChanged += Reset;
    }

    private void Update()
    {
        if (pointerOn && EditModeManager.i.playMode && !active && Input.GetMouseButtonDown(0))
        {
            animator.Play("Activate Lever");
            GameManager.i.lever = true;
            active = true;
        }
    }

    public void LeverUp()
    {
        LeverUpChanged?.Invoke();
    }

    private void OnDestroy()
    {
        EditModeManager.i.OnEditModeChanged -= Reset;
    }

    public void Reset()
    {
        animator.Play("None");
        GameManager.i.lever = false;
        active = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Pointer"))
        {
            pointerOn = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pointer"))
        {
            pointerOn = false;
        }
    }

}

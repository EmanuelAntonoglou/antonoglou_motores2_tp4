using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float snapDistance = 1f;
    public LayerMask obstacleMask;
    [NonSerialized] public bool playerDied = false;
    [NonSerialized] public Vector3 lastDirection = Vector3.zero;

    private Vector3 originalPos;
    private Quaternion originalRot;

    public List<Vector3> tilesToMove = new List<Vector3>();
    private Vector3 targetPosition;
    private Vector3 currentDirection;
    private bool moving = false;
    private bool reverse = false;

    [Header("Components")]
    [SerializeField] private Animator animator;

    void Start()
    {
        originalPos = transform.position;
        originalRot = transform.rotation;
        targetPosition = transform.position;
        GameManager.i.player.OnPlayerMoved += ExecuteInitialMove;
        GameManager.i.player.OnEnemyCancelMovement += AvoidMovement;
        EditModeManager.i.OnEditModeChanged += Reset;
    }

    private void OnDestroy()
    {
        GameManager.i.player.OnPlayerMoved -= ExecuteInitialMove;
        GameManager.i.player.OnEnemyCancelMovement -= AvoidMovement;
        EditModeManager.i.OnEditModeChanged -= Reset;
    }

    public void Reset()
    {
        if (GameManager.i.player != null)
        {
            tilesToMove.Clear();
            moving = false;
            transform.position = originalPos;
            transform.rotation = originalRot;
            lastDirection = Vector3.zero;
            animator.Play("Idle");
        }
    }

    private void AvoidMovement()
    {
        StopAllCoroutines();
        tilesToMove.Clear();
        moving = false;
        playerDied = false;
    }

    IEnumerator MoveToDestinationCoroutine(Vector3 direction)
    {
        if (!Physics.Raycast(transform.position, direction, snapDistance, obstacleMask))
        {
            targetPosition = transform.position + direction * snapDistance;
            moving = true;
            animator.Play("Run");
            RotatePlayer(direction);

            while (Vector3.Distance(transform.position, targetPosition) > 0.01f)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
                yield return null;
            }

            transform.position = targetPosition;
            moving = false;
            animator.Play("Idle");

            // Set the current direction to the last direction moved
            currentDirection = direction;
            lastDirection = currentDirection; // Actualiza lastDirection aqu�

            tilesToMove.RemoveAt(0); // Remove the current move from the list

            // Execute the next move if there are any left
            if (tilesToMove.Count > 0)
            {
                ExecuteMoves();
            }
        }
        else
        {
            reverse = !reverse;
            tilesToMove.Add(currentDirection * snapDistance); // Add the same direction again
            ExecuteMoves();
        }
    }

    public void ExecuteInitialMove()
    {
        if (!moving)
        {
            Vector3 initialMoveDirection = transform.forward;
            lastDirection = initialMoveDirection;
            tilesToMove.Add(initialMoveDirection * snapDistance);
            ExecuteMoves();
        }
    }

    public void ExecuteMoves()
    {
        if (tilesToMove.Count > 0)
        {
            Vector3 direction = tilesToMove[0];
            StartCoroutine(MoveToDestinationCoroutine(direction));
        }
    }

    private void RotatePlayer(Vector3 direction)
    {
        transform.rotation = Quaternion.LookRotation(direction);
    }
}

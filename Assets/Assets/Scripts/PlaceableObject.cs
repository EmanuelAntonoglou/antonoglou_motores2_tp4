using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class PlaceableObject : MonoBehaviour
{
    [Header("Edit Mode Data")]
    [NonSerialized] public bool placed = false;
    [NonSerialized] public Vector3 originalPos;
    [NonSerialized] public bool pointerEntered = false;

    [Header("Components")]
    [NonSerialized] public TileController tile;

    private void Start()
    {
        EditModeManager.OnResetEditState += ChangeToOriginalState;
    }

    private void OnDestroy()
    {
        EditModeManager.OnResetEditState -= ChangeToOriginalState;
    }

    private void Update()
    {
        if (!EditModeManager.i.playMode && EditModeManager.i.currentInstance == null)
        {
            if (pointerEntered && Input.GetMouseButtonDown(0) && placed && gameObject.name != "T_Ice(Clone)")
            {
                gameObject.transform.Rotate(0, 90, 0);
            }
            if (pointerEntered && Input.GetMouseButton(1))
            {
                Type tileType = GetComponent<Tile>().GetType();
                FieldInfo indexField = tileType.GetField("index", BindingFlags.Static | BindingFlags.Public);

                if (indexField != null && indexField.FieldType == typeof(int))
                {
                    int index = (int)indexField.GetValue(null);
                    LevelData.TileData tileData = GameManager.i.levelData.levelTiles[index];
                    tileData.actualAmount++;
                    GameManager.i.levelData.levelTiles[index] = tileData;
                }

                DestroyPlaceableObject();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Tile"))
        {
            TileController triggerTile = other.GetComponent<TileController>();
            if (triggerTile != null && !triggerTile.taken)
            {
                EditModeManager.i.canPlace = true;
                tile = triggerTile;
            }

        }
        if (other.CompareTag("Pointer"))
        {
            pointerEntered = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Tile"))
        {
            EditModeManager.i.canPlace = false;
        }
        if (other.CompareTag("Pointer"))
        {
            pointerEntered = false;
        }
    }

    public void ChangeToOriginalState()
    {
        transform.position = originalPos;
        gameObject.SetActive(true);
    }

    public void DestroyPlaceableObject()
    {
        if (tile != null)
        {
            tile.DeleteTile();
        }
        Destroy(gameObject);
    }

}

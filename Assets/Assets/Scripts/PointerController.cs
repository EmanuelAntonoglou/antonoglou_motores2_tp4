using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerController : MonoBehaviour
{
    [NonSerialized] public static PointerController i;

    [Header("Pointer Data")]
    [SerializeField] private float snapValue;
    [SerializeField] public Vector3 snapOffset;
    [SerializeField] private float ZDepth;
    [NonSerialized] public Tile currentTile;

    private void Awake()
    {
        CrearSingleton();
    }

    void Update()
    {
        MovePointerWithMouse();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Tile>() != null)
        {
            currentTile = other.GetComponent<Tile>();
        }
    }

    private void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void MovePointerWithMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = ZDepth;
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

        //Snap
        float snappedX = Mathf.Round(worldPosition.x / snapValue) * snapValue;
        float snappedZ = Mathf.Round(worldPosition.z / snapValue) * snapValue;

        Vector3 finalPos = new Vector3(snappedX + snapOffset.x, snapOffset.y, snappedZ + snapOffset.z);
        gameObject.transform.position = finalPos;
    }

}
